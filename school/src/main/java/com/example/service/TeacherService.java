package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.model.Teacher;
import com.example.repository.TeacherRepository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class TeacherService {

	@Autowired
	private TeacherRepository teacherRepository;

	public List<Teacher> findAll() {
		return teacherRepository.findAll();
	}
	
	public Optional<Teacher> findOne(Integer id) {
		return teacherRepository.findById(id);
	}
	
	@Transactional(readOnly = false)
	public Teacher save(Teacher entity) {
		return teacherRepository.save(entity);
	}

	@Transactional(readOnly = false)
	public void delete(Teacher entity) {
		teacherRepository.delete(entity);
	}

}
	
