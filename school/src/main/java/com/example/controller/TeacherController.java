package com.example.controller;

import java.util.List;

import javax.validation.Valid;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.model.Module;
import com.example.model.Teacher;
import com.example.service.ModuleService;
import com.example.service.TeacherService;

@Controller
@RequestMapping("/teachers")
public class TeacherController {
	
	@Autowired
	private TeacherService teacherService;
	
	@Autowired
	private ModuleService moduleService;
	
	@GetMapping
	public String index(Model model) {
		List<Teacher> all = teacherService.findAll();
		model.addAttribute("listTeacher", all);
		model.addAttribute("");
		return "teacher/index";
	}
	
	@GetMapping("/{id}")
	public String show(Model model, @PathVariable("id") Integer id) {
		if (id != null) {
			Teacher teacher = teacherService.findOne(id).get();
			model.addAttribute("teacher", teacher);
		}
		return "teacher/show";
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping(value = "/new")
	public String create(Model model, @ModelAttribute Teacher entityTeacher, @ModelAttribute Module entityModule) {
		// model.addAttribute("module", entityModule);
		
		return "teacher/form";
	}
	
	@Secured("ROLE_ADMIN")
	@PostMapping
	public String create(@Valid @ModelAttribute Teacher entity, BindingResult result, RedirectAttributes redirectAttributes) {
		Teacher teacher = null;
		try {
			teacher = teacherService.save(entity);
			redirectAttributes.addFlashAttribute("success", MSG_SUCESS_INSERT);
		} catch (Exception e) {
			System.out.println("Exception:: exception");
			e.printStackTrace();
			redirectAttributes.addFlashAttribute("error", MSG_ERROR);
		}catch (Throwable e) {
			System.out.println("Throwable:: exception");
			e.printStackTrace();
			redirectAttributes.addFlashAttribute("error", MSG_ERROR);
		}
		return "redirect:/teachers/" + teacher.getId();
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping("/{id}/edit")
	public String update(Model model, @PathVariable("id") Integer id) {
		
		try {
			if (id != null) {
				List<Module> all = moduleService.findAll();
				model.addAttribute("modules", all);
				
				Teacher entity = teacherService.findOne(id).get();
				model.addAttribute("teacher", entity);
			}
		} catch (Exception e) {
			throw new ServiceException(e.getMessage());
		}
		return "teacher/form";
	}
	
	@Secured("ROLE_ADMIN")
	@PutMapping
	public String update(@Valid @ModelAttribute Teacher entity, BindingResult result, 
			             RedirectAttributes redirectAttributes) {
		Teacher teacher = null;
		try {
			teacher = teacherService.save(entity);
			redirectAttributes.addFlashAttribute("success", MSG_SUCESS_UPDATE);
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", MSG_ERROR);
			e.printStackTrace();
		}
		return "redirect:/teacher/" + teacher.getId();
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping("/{id}/delete")
	public String delete(@PathVariable("id") Integer id, RedirectAttributes redirectAttributes) {
		try {
			if (id != null) {
				Teacher entity = teacherService.findOne(id).get();
				teacherService.delete(entity);
				redirectAttributes.addFlashAttribute("success", MSG_SUCESS_DELETE);
			}
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("error", MSG_ERROR);
			throw new ServiceException(e.getMessage());
		}
		return "redirect:/teachers/";
	}
	
	private static final String MSG_SUCESS_INSERT = "Teacher inserted successfully.";
	private static final String MSG_SUCESS_UPDATE = "Teacher successfully changed.";
	private static final String MSG_SUCESS_DELETE = "Deleted Teacher successfully.";
	private static final String MSG_ERROR = "Error when trying to insert a teacher!";


}
