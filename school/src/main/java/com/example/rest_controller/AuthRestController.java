package com.example.rest_controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Authentication;

@CrossOrigin(origins={ "http://localhost:3000", "http://localhost:4200", "http://localhost:8081" })
@RestController
@RequestMapping("/api")
public class AuthRestController {

    @GetMapping(path = "/basicauth")
    public Authentication helloWorldBean() {
        return new Authentication("You are authenticated");
    }   
}