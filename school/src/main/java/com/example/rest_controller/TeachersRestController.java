package com.example.rest_controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Teacher;
import com.example.service.TeacherService;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200", "http://localhost:8081" })
@RestController
@RequestMapping("/api")
public class TeachersRestController {
	
	@Autowired
	private TeacherService teacherService;
	
	@GetMapping("/teachers/{id}")
	public ResponseEntity<Teacher> show(@PathVariable("id") Integer id) throws Exception {
		Optional<Teacher> teacher = teacherService.findOne(id);
		
		if(teacher.isPresent()) {
			return ResponseEntity.ok().body(teacher.get());
		} else return ResponseEntity.notFound().build();
	}
	
	@GetMapping("/teachers")
	public ResponseEntity<List<Teacher>> showAll() {
		return ResponseEntity.ok().body(teacherService.findAll());
	}
	
	@PostMapping("/teachers")
	public ResponseEntity<Teacher> create(@Valid @RequestBody Teacher entity) {
		return ResponseEntity.ok(teacherService.save(entity));
	}
	
	@PutMapping("teachers/{id}")
	public Teacher update(@Valid @RequestBody Teacher entity) {
		return teacherService.save(entity);
	}
	
	@DeleteMapping("teachers/{id}")
	public ResponseEntity<Teacher> delete(@PathVariable("id") Integer id) {
		Optional<Teacher> teacher = teacherService.findOne(id);
		
		if(teacher.isPresent()) {
			teacherService.delete(teacher.get());
			return ResponseEntity.noContent().build();
		} else return ResponseEntity.notFound().build();
	}
}
