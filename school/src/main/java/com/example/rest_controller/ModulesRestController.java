package com.example.rest_controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Module;
import com.example.service.ModuleService;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200", "http://localhost:8081" })
@RestController
@RequestMapping("/api")
public class ModulesRestController {
	
	@Autowired
	private ModuleService moduleService;
	
	@GetMapping("/modules/{id}")
	public ResponseEntity<Module> show(@PathVariable("id") Integer id) throws Exception {
		Optional<Module> module = moduleService.findOne(id);
		
		if(module.isPresent()) {
			return ResponseEntity.ok().body(module.get());
		} else return ResponseEntity.notFound().build();
	}
	
	@GetMapping("/modules")
	public ResponseEntity<List<Module>> showAll() {
		return ResponseEntity.ok().body(moduleService.findAll());
	}
	
	@PostMapping("/modules")
	public ResponseEntity<Module> create(@Valid @RequestBody Module entity) {
		return ResponseEntity.ok(moduleService.save(entity));
	}
	
	@PutMapping("modules/{id}")
	public Module update(@Valid @RequestBody Module entity) {
		return moduleService.save(entity);
	}
	
	@DeleteMapping("modules/{id}")
	public ResponseEntity<Module> delete(@PathVariable("id") Integer id) {
		Optional<Module> module = moduleService.findOne(id);
		
		if(module.isPresent()) {
			moduleService.delete(module.get());
			return ResponseEntity.noContent().build();
		} else return ResponseEntity.notFound().build();
	}
}
