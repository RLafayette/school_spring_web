package com.example.rest_controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Student;
import com.example.service.StudentService;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200", "http://localhost:8081" })
@RestController
@RequestMapping("/api")
public class StudentsRestController {
	
	@Autowired
	private StudentService studentService;
	
	@GetMapping("/students/{id}")
	public ResponseEntity<Student> show(@PathVariable("id") Integer id) throws Exception {
		Optional<Student> student = studentService.findOne(id);
		
		if(student.isPresent()) {
			return ResponseEntity.ok().body(student.get());
		} else return ResponseEntity.notFound().build();
	}
	
	@GetMapping("/students")
	public ResponseEntity<List<Student>> showAll() {
		return ResponseEntity.ok().body(studentService.findAll());
	}
	
	@PostMapping("/students")
	public ResponseEntity<Student> create(@Valid @RequestBody Student entity) {
		return ResponseEntity.ok(studentService.save(entity));
	}
	
	@PutMapping("students/{id}")
	public Student update(@Valid @RequestBody Student entity) {
		return studentService.save(entity);
	}
	
	@DeleteMapping("students/{id}")
	public ResponseEntity<Student> delete(@PathVariable("id") Integer id) {
		Optional<Student> student = studentService.findOne(id);
		
		if(student.isPresent()) {
			studentService.delete(student.get());
			return ResponseEntity.noContent().build();
		} else return ResponseEntity.notFound().build();
	}
}
